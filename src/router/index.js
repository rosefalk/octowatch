import Vue from 'vue'
import Router from 'vue-router'
import Octowatch from '@/components/octowatch'
import Settings from '@/components/settings'
import Help from '@/components/help'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Octowatch',
      component: Octowatch
    }, {
      path: '/settings',
      name: 'Settings',
      component: Settings
    }, {
      path: '/help',
      name: 'Help',
      component: Help
    }
  ]
})
